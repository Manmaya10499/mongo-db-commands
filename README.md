# mongo-db-commands


## creating collection

```
db.createCollection('collection_name')
```

## inserting one row documents

```
db.posts.insert([
    {
        title:"post demo",
        post:[ {user: "userOne", name:"nameOne", date:Date() }, {user: "userTwo", name:"nameTwo", date:Date()}],
        tags:["java","oops","procedural","scripting"]
    }
])
```
## inserting more than one row

```
db.posts.insertMany([
    {
        title:"first post",
        body:" my first post",
        category : "technology",
        comments : [
            {
                comment_id : "10" ,
                comment_body : "random text inside commentOne"
            },

            {
                comment_id : "11" ,
                comment_body : "random text inside commentTwo"
            }
        ],
        tags : ["tagOne" , "tagTwo" ],
        date: Date(),
        Obj : {
                id: 123,
                title : "subPost inside post1",
                body : "subPost body of post1" 
              }
    },
    {
        title:"second post",
        body:" my second post",
        category : "history",
        comments : [
            {
                comment_id : "20" ,
                comment_body : "western history"
            },

            {
                comment_id : "21" ,
                comment_body : "indian history"
            }
        ],
        tags : ["tagOne" , "tagTwo" ],
        date: Date(),
        Obj : {
                id: 124,
                title : "subPost inside post2",
                body : "subPost body of post2" 
              }
    },
    {
        title:"third post",
        body:" my third post",
        category : "political science",
        comments : [
            {
                comment_id : "40" ,
                comment_body : "political scenario before independence"
            },

            {
                comment_id : "41" ,
                comment_body : "political scenario before independence"
            }
        ],
        tags : ["tagOne" , "tagTwo" ],
        date: Date(),
        Obj : {
                id: 125,
                title : "subPost inside post3",
                body : "subPost body of post3" 
              }
    }

])
```
## get all rows

```
db.posts.find()
```
## get all rows with formatted output

```
db.posts.find().pretty()
```

## find rows according to given constraints

```
db.posts.find({ category: 'technology'})
```

## sort rows

```
_asc
db.posts.find().sort({ title : 1 }).pretty()

_desc
db.posts.find().sort({ title : -1 }).pretty()
```
## count rows
```
db.posts.find().count()
db.posts.find({ category: 'history'}).count()
```

## limit Rows

```
db.posts.find().limit(3).pretty()
```

## chaining

```
db.posts.find().limit(2).sort({ title: 1 }).pretty()
```
## foreach

```
db.posts.find().forEach(function(doc) {
  print(doc.title)
})
```

## find One Row

```
db.posts.findOne({ category: 'geography' })
```



 





